/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metroparis;
import graphbase.Edge;
import graphbase.Graph;
import graphbase.GraphAlgorithms;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;



/**
 *
 * @author pmteixeira
 */
public class MetroParis {
    
    public static inputFiles io_interface = new inputFiles();
    // Task 5
    public static Graph<String, String> TimeMap = new Graph<>(true);
    // Task 4
    public static Graph<String, String> PathMap = new Graph<>(true);
    
    public static List<Stations> parisStations = new ArrayList<>();
    public static List<Connections> parisConnections = new ArrayList<>();
    public static List<Line> parisLine = new ArrayList<>();
    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    
    static void addStation(Stations param)
    {
        parisStations.add(param);
    }
    
    static void addConnection(Connections param)
    {
        parisConnections.add(param);
    }
    
    static void addLine(Line param)
    {
        parisLine.add(param);
    }
    
    static void displayStations()
    {
        System.out.println("\nPath -> displayStations() Function Called\n");
        parisStations.forEach((station) -> {
            station.display();
        });
    }
    
    static void displayConnections()
    {
        System.out.println("\nPath -> displayConnections() Function Called\n");
        parisConnections.forEach((connection) -> {
            connection.display();
        });
    }
    
    static void displayLine()
    {
        System.out.println("\nPath -> displayLine() Function Called\n");
        parisLine.forEach((line) -> {
            line.display();
        });
    }
    
    // Alínea 2 It's built like BFS
    public static<V, E> int allConnected(Graph<V, E> g, V vert) {

        // Verify a valid station name
        if (!g.validVertex(vert)) {
            return -1;
        }

        // Unconnected Station Count
        int unconnect_cnt = 0;
        LinkedList<V> qbfs = new LinkedList<>();
        LinkedList<V> qaux = new LinkedList<>();
        boolean[] visited = new boolean[g.numVertices()];  //default initializ.: false

        qbfs.add(vert);
        qaux.add(vert);
        int vKey = g.getKey(vert);
        visited[vKey] = true;

        // BFS Body
        while (!qaux.isEmpty()) {
            vert = qaux.remove();
            // Find all ways
            for (Edge<V, E> edge : g.outgoingEdges(vert)) {
                V vAdj = g.opposite(vert, edge);
                vKey = g.getKey(vAdj);
                if (!visited[vKey]) {
                    qbfs.add(vAdj);
                    qaux.add(vAdj);
                    visited[vKey] = true;
                }
            }
        }
        // Search All connections and find unconnected vertex count
        for (int i = 0; i < g.numVertices(); i++)
            if(visited[i] == false)
                unconnect_cnt++;
        return unconnect_cnt;
    }
    
    // Alínea 4  e Alínea 5 shortestPath.
    public static <V, E> Path goStationsSearch(Graph<V, E> g, V vert, V vDest)
    {
        Path task_path = new Path();
        LinkedList<V> shortPath = new LinkedList<>();
        double lenpath = 0;
        lenpath = GraphAlgorithms.shortestPath(g, vert, vDest, shortPath);
        // Get Path from Station Name String Array
        task_path.setInformationFromStations((List<String>) shortPath);
        if(lenpath == 0)
        {
            System.out.println("No path");
        }
        
        return task_path;
    }
    
    // Alínea 6 - BreadthFirstSearch based method
    public static <V, E> LinkedList<V> lineChangeSearch(Graph<V, E> g, V vert, V vDest) {

        if (!g.validVertex(vert) || !g.validVertex(vDest)) {
            return null;
        }

        LinkedList<V> qbfs = new LinkedList<>();
        int buf_pr[] = new int[g.numVertices()];
        LinkedList<V> buf_line = new LinkedList<>();
        int buf_pos = 0, base_pos = 0;
        
        // Priority Queue to find min line changes
        PriorityQueue<Integer> qaux = new PriorityQueue<>();
        LinkedList<V> ret_route = new LinkedList<>();
        boolean[] visited = new boolean[g.numVertices()];  //default initializ.: false

        qbfs.add(vert);
        buf_pr[buf_pos] = -1;
        buf_line.add((V) "Base++");
        qaux.add(new Integer(0));
        int vKey = g.getKey(vert);
        visited[vKey] = true;

        // Search with Priority Queue for small line changes.
        while (!qaux.isEmpty()) {
            
            Integer mother_v = qaux.remove();
            vert = qbfs.get(base_pos);
            
            for (Edge<V, E> edge : g.outgoingEdges(vert)) {
                V vAdj = g.opposite(vert, edge);
                vKey = g.getKey(vAdj);
                
                if (!visited[vKey]) {
                    buf_pos ++;
                    // Set the parent to find the route.
                    buf_pr[buf_pos] = base_pos;
                    qbfs.add(vAdj);
                    buf_line.add((V) edge.getElement());
                    Integer new_set;
                    // Find the line changes
                    if(edge.getElement().equals(buf_line.get(base_pos)))
                        new_set = mother_v;
                    else
                        new_set = mother_v + 1;
                    qaux.add(new_set);
                    visited[vKey] = true;
                    
                    if (vAdj.equals(vDest))
                    {
                        //System.out.println("Total Line Change: " + new_set + "\n");
                        // Find Parent for make return route strings.
                        while(buf_pos >= 0)
                        {
                            ret_route.addFirst(qbfs.get(buf_pos));
                            buf_pos = buf_pr[buf_pos];
                        }
                        return ret_route;
                    }
                }
            }
            base_pos ++;
        }
        return qbfs;
    }
    
    // Alínea 7 BSF Expandido para procura intermédia de estações //Expanded BSF Search For Intermediate Stations search.
    public static <V, E> LinkedList<V> searchIntermediateStation(Graph<V, E> g, V vert, V vDest, LinkedList<V> interStations){
        
        // Verify the stations.
        if (!g.validVertex(vert) || !g.validVertex(vDest)) {
            return null;
        }
        
        int[] interArr = new int[1024];
        int interCnt = 0;
        // Verify all intermediate stations
        while(!interStations.isEmpty())
        {
            V data = interStations.getFirst();
            interStations.removeFirst();
            if (!g.validVertex(data)) {
                return null;
            }
            interArr[interCnt] = g.getKey(data);
            interCnt ++;
        }
        
        LinkedList<V> qbfs = new LinkedList<>();
        
        // Buf Size will be more the stations count because of this BFS Algorithm.
        int buf_pr[] = new int[g.numVertices() * 1024];
        LinkedList<V> buf_line = new LinkedList<>();
        int buf_pos = 0, base_pos = 0;
        
        // Priority Queue to Find the best way.
        PriorityQueue<Double> qaux = new PriorityQueue<>();
        LinkedList<V> ret_route = new LinkedList<>();
        LinkedList<V> temp_route = new LinkedList<>();
        boolean[] visited = new boolean[g.numVertices() * 1024];  //default initializ.: false

        qbfs.add(vert);
        // Set Parent -1 to break out.
        buf_pr[buf_pos] = -1;
        buf_line.add((V) "Base++");
        qaux.add(new Double(0));
        int vKey = g.getKey(vert);
        visited[vKey] = true;
        
        // Original BFS can't contain same vertex(station) But here for intermediate statons search can push same stations anytime except now route.
        while (!qaux.isEmpty()) {
            Double mother_v = qaux.remove();
            vert = qbfs.get(base_pos);
            for (Edge<V, E> edge : g.outgoingEdges(vert)) {
                V vAdj = g.opposite(vert, edge);
                vKey = g.getKey(vAdj);
                // temp_route is the now route and visited_flag is useful for this algorithm
                boolean visited_flag = false;
                int temp_pos = base_pos;
                while(temp_pos >= 0)
                {
                    temp_route.addFirst(qbfs.get(temp_pos));
                    temp_pos = buf_pr[temp_pos];
                }   
                while(!temp_route.isEmpty())
                {
                    V temp_val = temp_route.getFirst();
                    temp_route.removeFirst();
                    // Find if there is vKey.
                    if(g.getKey(temp_val) == vKey)
                        visited_flag = true;
                }
                if (!visited_flag) {
                    if (vAdj.equals(vDest)) // If Arrived destination
                    {
                        
                        int t_vis = 1;
                        // Find the intermediate stations.
                        for ( int i = 0; i < interCnt; i++)
                        {
                            int this_value = interArr[i];
                            
                            int vst = 0;
                            int temp_post = base_pos;
                            while(temp_post >= 0)
                            {
                                temp_route.addFirst(qbfs.get(temp_post));
                                temp_post = buf_pr[temp_post];
                            }   
                            while(!temp_route.isEmpty())
                            {
                                V temp_val = temp_route.getFirst();
                                temp_route.removeFirst();
                                if(g.getKey(temp_val) == this_value)
                                    vst = 1;
                            }
                            if(vst == 0)
                                t_vis = 0;
                        }
                        // Not Passed Intermediate Stations.
                        if(t_vis == 0)
                            continue;
                        // Arrived with Intermediate Stations
                        buf_pos ++;
                        buf_pr[buf_pos] = base_pos;
                        qbfs.add(vAdj);
                        buf_line.add((V) edge.getElement());

                        Double new_set = mother_v + edge.getWeight();
                        qaux.add(new_set);

                        visited[vKey] = true;
                        //System.out.println("Total Time Cost: " + new_set + "\n");
                        // Find the route Stations Names Strings.
                        while(buf_pos >= 0)
                        {
                            ret_route.addFirst(qbfs.get(buf_pos));
                            buf_pos = buf_pr[buf_pos];
                        }
                        return ret_route;
                    }
                    else
                    {
                        buf_pos ++;
                        // Set Parent Information for route.
                        buf_pr[buf_pos] = base_pos;
                        qbfs.add(vAdj);
                        buf_line.add((V) edge.getElement());

                        Double new_set = mother_v + edge.getWeight();
                        qaux.add(new_set);

                        visited[vKey] = true;
                    }
                }
            }
            base_pos ++;
        }
        return qbfs;
    }
    
    public static void main(String[] args) throws FileNotFoundException {
        
        io_interface.readFiles(false);
        
        //displayStations();
        
        //displayConnections();
        
        //displayLine();
        
        // Alínea 2 Base on BFS
        System.out.println("\nTest of Task 2 Verify the Graph is Connected\n");
        int uncnt = allConnected(PathMap, "Cadet");
        switch (uncnt) {
            case -1:
                System.out.println("Invalid Station Name");
                break;
            case 0:
                System.out.println("Connected All");
                break;
            default:
                System.out.println("Not Connected Station Count: " + uncnt);
                break;
        }
        
        // Alínea 4
        System.out.println("\nTest of Task 4 From Chatelet to Alesia\n");
        Path path_4 = new Path();
        path_4 = goStationsSearch(PathMap, "Cadet", "Alesia");
        path_4.displayInformation();
        
        // Alínea 5
        System.out.println("\nTest of Task 5 From Chatelet to Alesia\n");
        Path path_5 = new Path();
        path_5 = goStationsSearch(TimeMap, "Liege", "Alesia");
        path_5.displayInformation();
        
        // Alínea 6 - Base on BFS
        System.out.println("\nTest of Task 6 From Chatelet to Alesia\n");
        Path path_6 = new Path();
        LinkedList<String> shortLine = new LinkedList<>();
        shortLine = lineChangeSearch(TimeMap, "Liege", "Porte de Vanves");
        if(shortLine.isEmpty())
        {
            System.out.println("No path");
        }
        path_6.setInformationFromStations((List<String>) shortLine);
        path_6.displayInformation();
        
        // Task 7 Base on Expanded BFS Search
        System.out.println("\nTest of Task 7 From Chatelet to Alesia With Montparnasse Bienvenue, Edgar Quinet\n");

        Path path_7 = new Path();
        LinkedList<String> shortInterStations = new LinkedList<>();
        
        LinkedList<String> interStationsList = new LinkedList<>();
        interStationsList.add("Montparnasse Bienvenue"); //Montparnasse Bienvenue
        interStationsList.add("Edgar Quinet");//Edgar Quinet
        
        shortInterStations = searchIntermediateStation(TimeMap, "Chatelet", "Alesia", interStationsList);
        if(shortInterStations.isEmpty())
        {
            System.out.println("No path");
        }
        
        path_7.setInformationFromStations((List<String>) shortInterStations);
        path_7.displayInformation();

    }

}
