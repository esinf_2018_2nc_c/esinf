/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metroparis;

/**
 *
 * @author pmteixeira
 */
public class Line {
    public String Default_Code;
    public String StationName;
    
    public Line()
    {
        
    }

    Line(String inp_dc, String inp_sn) 
    {
        Default_Code = inp_dc;
        StationName = inp_sn;
    }
    
    public void SetValue(String inp_dc, String inp_sn)
    {
        Default_Code = inp_dc;
        StationName = inp_sn;
    }
    
    public void display()
    {
        System.out.println(Default_Code + " - " + StationName);
    }
}
