/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metroparis;

/**
 *
 * @author pmteixeira
 */
public class Connections {
    
    public String Line;
    public String Station_1;
    public String Station_2;
    private double Time;
    
    public Connections()
    {
        
    }

    Connections(String inp_ln, String inp_st1, String inp_st2, double inp_tm) 
    {
        Line = inp_ln;
        Station_1 = inp_st1;
        Station_2 = inp_st2;
        Time = inp_tm;
    }
    
    public void SetValue(String inp_ln, String inp_st1, String inp_st2, double inp_tm)
    {
        Line = inp_ln;
        Station_1 = inp_st1;
        Station_2 = inp_st2;
        Time = inp_tm;
    }
    
    public void display()
    {
        System.out.println(Line + " - " + Station_1 + " - " + Station_2 + " - " + Time);
    }
}
