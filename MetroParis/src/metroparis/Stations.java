/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metroparis;

/**
 *
 * @author pmteixeira
 */
public class Stations {
    public String StationName;
    private double Latitude;
    private double Longitude;
    
    public Stations()
    {
        
    }

    Stations(String inp_sn, double inp_lat, double inp_long) {
        StationName = inp_sn;
        Latitude = inp_lat;
        Longitude = inp_long;
    }
    
    public void SetValue(String inp_sn, double inp_lat, double inp_long)
    {
        StationName = inp_sn;
        Latitude = inp_lat;
        Longitude = inp_long;
    }
    
    public void display()
    {
        System.out.println(StationName + " - " + Latitude + " - " +Longitude);
    }
}
