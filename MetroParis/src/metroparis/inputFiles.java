/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metroparis;

import graphbase.Graph;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
/**
 *
 * @author pmteixeira
 */
public class inputFiles {
    
    public inputFiles() {
    }
    
    public void readFiles(boolean testFlag) throws FileNotFoundException {
        // Here read Coordinate File.
        if(!testFlag)
            System.out.print("\nReading coordinate.csv\n");
            
        try (Scanner coordFile = new Scanner(new File("coordinates.csv"))) {
            if(testFlag)
                System.out.print("\nSuccess Opening coordinates.csv\n");
            coordFile.useDelimiter(";");
            while (coordFile.hasNext()) {
                String StationName = coordFile.next();
                double Latitude = Double.parseDouble(coordFile.next());
                double Longitude = Double.parseDouble(coordFile.nextLine().replaceAll(";", ""));
                
                if(!testFlag)
                    System.out.println(StationName + " - " + Latitude + " - " +Longitude);
                
                metroparis.MetroParis.addStation(new Stations(StationName, Latitude, Longitude));
                metroparis.MetroParis.TimeMap.insertVertex(StationName);
                metroparis.MetroParis.PathMap.insertVertex(StationName);
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("\nCan not find coordinates.csv\n");
        }
        
        // Here read lines_and_stations File.
        if(!testFlag)
            System.out.print("\nReading lines_and_stations.csv\n");
        try (Scanner lineFile = new Scanner(new File("lines_and_stations.csv"))) {
            if(testFlag)
                System.out.print("\nSuccess Opening lines_and_stations.csv\n");
            lineFile.useDelimiter(";");
            while (lineFile.hasNext()) {
                String Default_Code = lineFile.next();
                String StationName = lineFile.nextLine().replaceAll(";", "");
                if(!testFlag)
                    System.out.println(Default_Code + " - " + StationName);
                
                metroparis.MetroParis.addLine(new Line(Default_Code, StationName));
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("\nCan not find lines_and_stations.csv\n");
        }
        
        // Here read connections File.
        if(!testFlag)
            System.out.print("\nReading connections.csv\n\n");
        try (Scanner connectFile = new Scanner(new File("connections.csv"))) {
            if(testFlag)
                System.out.print("\nSuccess Opening connections.csv\n");
            connectFile.useDelimiter(";");
            while (connectFile.hasNext()) {
                String Line = connectFile.next();
                String Station_1 = connectFile.next();
                String Station_2 = connectFile.next();
                int Time = Integer.parseInt(connectFile.nextLine().replaceAll(";", ""));
                 
                metroparis.MetroParis.addConnection(new Connections(Line, Station_1, Station_2, Time));
                // Task 5
                metroparis.MetroParis.TimeMap.insertEdge(Station_1, Station_2, Line, Time);
                // Task 4
                metroparis.MetroParis.PathMap.insertEdge(Station_1, Station_2, Line, 1);
                if(!testFlag)
                    System.out.println(Line + " - " + Station_1 + " - " + Station_2 + " - " + Time);
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("\nCan not find connections.csv\n");
        }
    }
    // Task 6 Line Conveting
}
