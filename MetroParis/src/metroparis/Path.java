/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metroparis;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author pmteixeira
 */
public class Path {
    
    public List<Stations> pathStations = new ArrayList<>();
    public List<Connections> pathConnections = new ArrayList<>();
    public List<Line> pathLine = new ArrayList<>();
    
    Path()
    {
        
    }
    
    public Path setInformationFromStations(List<String> stations)
    {
        Iterator<String> itSource = stations.iterator();
        String last_str = "";
        while (itSource.hasNext()) {
            String this_str = itSource.next();
            
            Iterator<Stations> itStations = MetroParis.parisStations.iterator();
            while(itStations.hasNext())
            {
                Stations this_station = itStations.next();
                if(this_station.StationName.equals(this_str) == true)
                {
                    pathStations.add(this_station);
                    break;
                }
            }
            
            
            
            
            Iterator<Connections> itConnections = MetroParis.parisConnections.iterator();
            while(itConnections.hasNext())
            {
                Connections this_connection = itConnections.next();
                if(this_connection.Station_1.equals(last_str) == true && this_connection.Station_2.equals(this_str) == true)
                {
                    pathConnections.add(this_connection);
                    
                    Iterator<Line> itLine = MetroParis.parisLine.iterator();
                    while(itLine.hasNext())
                    {
                        Line this_line = itLine.next();
                        if(this_line.StationName.equals(last_str) == true && this_line.Default_Code.equals(this_connection.Line) == true)
                        {
                            pathLine.add(this_line);
                            break;
                        }
                    }
                    
                    break;
                }
            }
            
            last_str = this_str;
        }
        
        return this;
    }
    
    public void displayInformation()
    {
        System.out.println("\nDisplay Path Information");
        
        System.out.println("\nDisplay Stations");
        
        pathStations.forEach((station) -> {
            station.display();
        });
        
        System.out.println("\nDisplay Connections");
        
        pathConnections.forEach((connection) -> {
            connection.display();
        });
        
        System.out.println("\nDisplay Line");
        
        pathLine.forEach((line) -> {
            line.display();
        });
        
    }
}
