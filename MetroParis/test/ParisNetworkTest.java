
import java.util.Iterator;
import java.util.LinkedList;
import graphbase.GraphAlgorithms;
import java.io.FileNotFoundException;
import metroparis.MetroParis;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author DEI-ISEP
 */
public class ParisNetworkTest {
    
    public ParisNetworkTest() {
    }

    @Before
    public void setUp() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of InputFile Class Task 1.
    */ 
    
    @Test
    public void testInputFile() throws FileNotFoundException {
        System.out.println("\nTest InputFiles ----- Start -----\n");
        
        MetroParis.io_interface.readFiles(true);
        //MetroParis.io_interface.readFiles(true);
        
        assertTrue("Test1 Has Balard",MetroParis.PathMap.validVertex("Balard") == true);

        assertTrue("Test2 Has Commerce",MetroParis.PathMap.validVertex("Commerce") == true);

        assertTrue("Test3 Has Laumiere",MetroParis.PathMap.validVertex("Laumiere") == true);

        assertTrue("Test4 Has realtest",MetroParis.PathMap.validVertex("realtest") == false);

        System.out.println("\nTest InputFiles ----- END -----\n");
    }
    
    /**
     * Test of Connectivity Task 2.
    */ 
    
    @Test
    public void testConnectivity()
    {
        System.out.println("\nTest Connectivity ----- Start -----\n");
        
        assertTrue("Test5 TimeMap Connectivity From La Defense", MetroParis.allConnected(MetroParis.TimeMap, "La Defense") == 0);
        
        assertTrue("Test6 TimeMap Connectivity From testreal", MetroParis.allConnected(MetroParis.TimeMap, "testreal") == -1);
        
        assertTrue("Test7 PathMap Connectivity From Liberte", MetroParis.allConnected(MetroParis.PathMap, "Liberte") == 0);
        
        assertTrue("Test8 PathMap Connectivity From testreal", MetroParis.allConnected(MetroParis.PathMap, "testreal") == -1);

        System.out.println("\nTest Connectivity - END\n");
    }
    
    /**
     * Test of Number of Stations - 4.
    */ 
    
    @Test
    public void testPathNumberOfStations()
    {
        System.out.println("\nTest Task 4 Path Number Of Stations ----- Start -----\n");
        
        LinkedList<String> shortPath = new LinkedList<>();
        double lenpath = 0;
        lenpath = GraphAlgorithms.shortestPath(MetroParis.PathMap, "Chatelet", "LX", shortPath);
        assertTrue("Test 9 Length path should be 0 if vertex does not exist", shortPath.isEmpty());

        lenpath = GraphAlgorithms.shortestPath(MetroParis.PathMap, "Chatelet", "Chatelet", shortPath);
        assertTrue("Test 10 Length path should be 1 if source and vertex are the same", shortPath.size() == 1);

        lenpath = GraphAlgorithms.shortestPath(MetroParis.PathMap, "Chatelet", "Alesia", shortPath);
        
        assertTrue("Test 11 Path between Chatelet and Alesia should be 12", lenpath == 12);

        Iterator<String> it = shortPath.iterator();

        assertTrue("Test 12 First in path should be Porto", it.next().compareTo("Chatelet") == 0);
        assertTrue("Test 13 then Cite", it.next().compareTo("Cite") == 0);
        assertTrue("Test 14 then Saint Michel", it.next().compareTo("Saint Michel") == 0);
        assertTrue("Test 15 then Odeon", it.next().compareTo("Odeon") == 0);

        System.out.println("\n Teste Alínea 5 Path Number Of Stations - END \n");
    }
    
    /**
     * Test 5.
    */ 
    
    @Test
    public void testPathTime()
    {
        System.out.println("\nTest Task 5 Path Time ----- Start -----\n");
        
        LinkedList<String> shortPath = new LinkedList<>();
        double lenpath = 0;
        lenpath = GraphAlgorithms.shortestPath(MetroParis.TimeMap, "Chatelet", "LX", shortPath);
        assertTrue("Test 16 Length path should be 0 if vertex does not exist", shortPath.isEmpty());

        lenpath = GraphAlgorithms.shortestPath(MetroParis.TimeMap, "Chatelet", "Chatelet", shortPath);
        assertTrue("Test 17 Length path should be 1 if source and vertex are the same", shortPath.size() == 1);

        lenpath = GraphAlgorithms.shortestPath(MetroParis.TimeMap, "Chatelet", "Alesia", shortPath);
        
        assertTrue("Test 18 Path between Chatelet and Alesia should be 17", lenpath == 13);

        Iterator<String> it = shortPath.iterator();

        assertTrue("Test 19 First in path should be Porto", it.next().compareTo("Chatelet") == 0);

        System.out.println("\n Teste Alínea 5 Path Time - END\n");
    }
    
    /**
     * Test of Number of Line Changes - Alínea 6.
    */ 
    @Test
    public void testPathNumberOfLineChanges()
    {
        System.out.println("\nTest Task 6 Path Number Of Line Changes ----- Start -----\n");
        
        LinkedList<String> shortPath = new LinkedList<>();
        double lenpath = 0;
        shortPath = MetroParis.lineChangeSearch(MetroParis.PathMap, "Chatelet", "LX");
        assertTrue("Test 20 Length path should be null if vertex does not exist", shortPath == null);
        
        shortPath = MetroParis.lineChangeSearch(MetroParis.PathMap, "Chatelet", "Alesia");
        assertTrue("Test 21 Path between Chatelet and Alesia should be 13", shortPath.size() == 13);

        Iterator<String> it = shortPath.iterator();

        assertTrue("Test 22 First in path should be Porto", it.next().compareTo("Chatelet") == 0);
        assertTrue("Test 23 then Cite", it.next().compareTo("Cite") == 0);
        assertTrue("Test 24 then Saint Michel", it.next().compareTo("Saint Michel") == 0);
        assertTrue("Test 25 then Odeon", it.next().compareTo("Odeon") == 0);

        System.out.println("\nTest Task 6 Path Number Of Line Changes ----- END -----\n");
    }
    
    /**
     * Test of Number of Line Changes Task 7.
    */ 
    
    @Test
    public void testTimeofIntermediate()
    {
        System.out.println("\nTest Task 7 Time of Intermediate Stations ----- Start -----\n");
        
        LinkedList<String> shortPath = new LinkedList<>();
        double lenpath = 0;
        
        LinkedList<String> interStationsList = new LinkedList<>();
        interStationsList.add("Liege");
        interStationsList.add("Edgar Quinet");
        
        shortPath = MetroParis.searchIntermediateStation(MetroParis.PathMap, "Chatelet", "LX", interStationsList);
        assertTrue("Test 23 Length path should be null if vertex does not exist", shortPath == null);
        
        shortPath = MetroParis.searchIntermediateStation(MetroParis.PathMap, "Chatelet", "Alesia", interStationsList);
        assertTrue("Test 24 Path between Chatelet and Alesia should be 13", shortPath.size() == 13);

        Iterator<String> it = shortPath.iterator();

        assertTrue("Test 25 First in path should be Porto", it.next().compareTo("Chatelet") == 0);
        assertTrue("Test 26 then Cite", it.next().compareTo("Cite") == 0);
        assertTrue("Test 27 then Saint Michel", it.next().compareTo("Saint Michel") == 0);
        assertTrue("Test 28 then Odeon", it.next().compareTo("Odeon") == 0);

        System.out.println("\nTest Task 7 Time of Intermediate Stations ----- END -----\n");
        
        
        System.out.println("\n\nTotal Test ----- Finished Tests -----\n");
    }
}
