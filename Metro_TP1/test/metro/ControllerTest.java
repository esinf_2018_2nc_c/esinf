/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro;

import java.io.IOException;
import java.util.ListIterator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pmteixeira
 */
public class ControllerTest {
    
    public ControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of inputEstacoes method, of class Controller.
     */
    @Test
    public void testInputEstacoes() throws IOException {
        System.out.println("inputEstacoes");
        String loc = "fx_estacoes.txt";
        Controller instance = new Controller();
        instance.inputEstacoes(loc);

    }

    /**
     * Test of inputViagens method, of class Controller.
     */
    @Test
    public void testInputViagens() throws IOException {
        System.out.println("inputViagens");
        String loc = "fx_viagens.txt";
        Controller instance = new Controller();
        instance.inputViagens(loc);

    }

    /**
     * Test of bilhetesTransgressao method, of class Controller.
     */
    @Test
    public void testBilhetesTransgressao() {
        System.out.println("bilhetesTransgressao");
        Controller instance = new Controller();
        instance.bilhetesTransgressao();
    }

    /**
     * Test of validaBilhetes method, of class Controller.
     */
    @Test
    public void testValidaBilhetes() throws IOException {
        System.out.println("validaBilhetes");
        int orig = 1;
        int dest = 10;
        String tipo = "z3";
        Controller instance = new Controller();
        instance.inputEstacoes("fx_estacoes.txt");
        instance.inputViagens("fx_viagens.txt");
        boolean expResult = false;
        boolean result = instance.validaBilhetes(orig, dest, tipo);
        assertEquals(expResult, result);
    }

   

    /**
     * Test of utilizadoresPorEstacao method, of class Controller.
     * @throws java.io.IOException
     */
    @Test
    public void testUtilizadoresPorEstacao() throws IOException {
        System.out.println("utilizadoresPorEstacao");
        Controller instance = new Controller();
        instance.inputEstacoes("fx_estacoes.txt");
        instance.inputViagens("fx_viagens.txt");
        instance.utilizadoresPorEstacao();
  
    }

    /**
     * Test of contaPassagens method, of class Controller.
     * @throws java.io.IOException
     */
    @Test
    public void testContaPassagens() throws IOException {
        System.out.println("contaPassagens");
        int estacao = 10;
        Controller instance = new Controller();
        instance.inputEstacoes("fx_estacoes_teste.txt");
        instance.inputViagens("fx_viagens_teste.txt");
        int expResult =1;
        int result = instance.contaPassagens(estacao);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of ordenaZonas method, of class Controller.
     */
    @Test
    public void testOrdenaZonas() throws IOException {
        System.out.println("ordenaZonas");
        Controller instance = new Controller();
        instance.inputEstacoes("fx_estacoes.txt");
        instance.inputViagens("fx_viagens.txt");
        instance.ordenaZonas();
        
        DoublyLinkedList<Zona> tmp = instance.getZonas();
        
        ListIterator<Zona> itz = tmp.listIterator();
        while(itz.hasNext())
        {
         
            Zona temp = itz.next();
            System.out.print("ZONA: "+ temp.getTipo()+" ");       
    }
    }
    /**
     * Test of getZonaByTipo method, of class Controller.
     */
    @Test
    public void testGetZonaByTipo() {
        System.out.println("getZonaByTipo");
        String tipo = "C1";
        Controller instance = new Controller();
        Zona expResult = new Zona("C1");
        instance.getZonas().addFirst(expResult);
        Zona result = instance.getZonaByTipo(tipo);
        assertEquals(expResult, result);
    }

    /**
     * Test of validaZonas method, of class Controller.
     */
    @Test
    public void testValidaZonas() {
        System.out.println("validaZonas");
        Zona zona = new Zona("C1");
        Controller instance = new Controller();
        instance.getZonas().addFirst(zona);
        boolean expResult = false;
        boolean result = instance.validaZonas(zona);
        assertEquals(expResult, result);
    }

    /**
     * Test of maiorSequenciaEstacoes method, of class Controller.
     */
    @Test
    public void testMaiorSequenciaEstacoes() {
        System.out.println("maiorSequenciaEstacoes");
        String tipo = "";
        Controller instance = new Controller();
        String expResult = "";
        String result = instance.maiorSequenciaEstacoes(tipo);
        assertEquals(expResult, result);

    }

   
}
