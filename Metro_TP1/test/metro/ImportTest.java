package metro;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import metro.DoublyLinkedList;
import metro.Estacao;
import metro.inputFile;
import metro.Bilhete;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 *
 * @author pmteixeira
 */
public class ImportTest {
    
    public ImportTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void importEstacoesTestNumEstacao() throws IOException {
         DoublyLinkedList<Estacao> estacoes = inputFile.importEstacoes(".\\fx_estacoes.txt");  
        
         Estacao IPO = new Estacao(2, "IPO", "C1");
         Estacao s_joao = new Estacao(1, "Hospital de S.Joao", "C1");
         Estacao casamusica = new Estacao(10, "Casa da Musica", "C4");
        
         
         DoublyLinkedList<Estacao> estacoesEsperadas = new DoublyLinkedList<>();
         estacoesEsperadas.addFirst(IPO);
         estacoesEsperadas.addFirst(s_joao);
         estacoesEsperadas.addFirst(casamusica);  
         
         assertEquals(estacoes.removeLast().getNumEstacao(), estacoesEsperadas.removeLast().getNumEstacao()); 
         assertEquals(estacoes.removeLast().getNumEstacao(), estacoesEsperadas.removeLast().getNumEstacao());
         assertEquals(estacoes.removeLast().getNumEstacao(), estacoesEsperadas.removeLast().getNumEstacao());      
    }
    
    @Test
    public void importEstacoesTestDescricao() throws IOException {
        DoublyLinkedList<Estacao> estacoes = inputFile.importEstacoes(".\\fx_estacoes.txt");  
        
         Estacao IPO = new Estacao(2, "IPO", "C1");
         Estacao s_joao = new Estacao(1, "Hospital de S.Joao", "C1");
         Estacao casamusica = new Estacao(10, "Casa da Musica", "C4");
        
         
         DoublyLinkedList<Estacao> estacoesEsperadas = new DoublyLinkedList<Estacao>();
         estacoesEsperadas.addFirst(IPO);
         estacoesEsperadas.addFirst(s_joao);
         estacoesEsperadas.addFirst(casamusica);
         
         
         assertEquals(estacoes.removeLast().getDescricao(), estacoesEsperadas.removeLast().getDescricao());
         assertEquals(estacoes.removeLast().getDescricao(), estacoesEsperadas.removeLast().getDescricao());
         assertEquals(estacoes.removeLast().getDescricao(), estacoesEsperadas.removeLast().getDescricao());

    }
    @Test
    public void importEstacoesTestZona() throws IOException {
        DoublyLinkedList<Estacao> estacoes = inputFile.importEstacoes(".\\fx_estacoes.txt");  
        
         Estacao IPO = new Estacao(2, "IPO", "C1");
         Estacao s_joao = new Estacao(1, "Hospital de S.Joao", "C1");
         Estacao casamusica = new Estacao(10, "Casa da Musica", "C4");
        
         
         DoublyLinkedList<Estacao> estacoesEsperadas = new DoublyLinkedList<Estacao>();
         estacoesEsperadas.addFirst(IPO);
         estacoesEsperadas.addFirst(s_joao);
         estacoesEsperadas.addFirst(casamusica);
         
         assertEquals(estacoes.removeLast().getZona(), estacoesEsperadas.removeLast().getZona());
         assertEquals(estacoes.removeLast().getZona(), estacoesEsperadas.removeLast().getZona());
         assertEquals(estacoes.removeLast().getZona(), estacoesEsperadas.removeLast().getZona());

    }
    
    @Test
    public void importViagensTestNumero() throws IOException{
        DoublyLinkedList<Bilhete> viagens = inputFile.importViagens(".\\fx_viagens.txt");  

        Bilhete s1 = new Bilhete(111222333, "Z2", 1, 10);
        Bilhete s2 = new Bilhete(111333222, "Z3", 1, 2);
        Bilhete s3 = new Bilhete(111444222, "Z2", 2, 1);
        
         
         DoublyLinkedList<Bilhete> viagensEsperadas = new DoublyLinkedList<Bilhete>();
         viagensEsperadas.addLast(s1);
         viagensEsperadas.addLast(s2);
         viagensEsperadas.addLast(s3);
        

         assertEquals(viagens.removeLast().getNumero(), viagensEsperadas.removeLast().getNumero());
         assertEquals(viagens.removeLast().getNumero(), viagensEsperadas.removeLast().getNumero());
         assertEquals(viagens.removeLast().getNumero(), viagensEsperadas.removeLast().getNumero());
         
    }
    
    
    
    @Test
    public void importViagensTestTipo() throws IOException{
        DoublyLinkedList<Bilhete> viagens = inputFile.importViagens(".\\fx_viagens.txt");  

        Bilhete s1 = new Bilhete(111222333, "Z2", 1, 10);
        Bilhete s2 = new Bilhete(111333222, "Z3", 1, 2);
        Bilhete s3 = new Bilhete(111444222, "Z2", 2, 1);
        
         
         DoublyLinkedList<Bilhete> viagensEsperadas = new DoublyLinkedList<Bilhete>();
         viagensEsperadas.addLast(s1);
         viagensEsperadas.addLast(s2);
         viagensEsperadas.addLast(s3);
        

         assertEquals(viagens.removeLast().getTipo(), viagensEsperadas.removeLast().getTipo());
         assertEquals(viagens.removeLast().getTipo(), viagensEsperadas.removeLast().getTipo());
         assertEquals(viagens.removeLast().getTipo(), viagensEsperadas.removeLast().getTipo());
         
    }
    
    @Test
    public void importViagensTestEstacaoOrig() throws IOException{
        DoublyLinkedList<Bilhete> viagens = inputFile.importViagens(".\\fx_viagens.txt");  

        Bilhete s1 = new Bilhete(111222333, "Z2", 1, 10);
        Bilhete s2 = new Bilhete(111333222, "Z3", 1, 2);
        Bilhete s3 = new Bilhete(111444222, "Z2", 2, 1);
        
         
         DoublyLinkedList<Bilhete> viagensEsperadas = new DoublyLinkedList<Bilhete>();
         viagensEsperadas.addLast(s1);
         viagensEsperadas.addLast(s2);
         viagensEsperadas.addLast(s3);
        

         assertEquals(viagens.removeLast().getEstacaoOrig(), viagensEsperadas.removeLast().getEstacaoOrig());
         assertEquals(viagens.removeLast().getEstacaoOrig(), viagensEsperadas.removeLast().getEstacaoOrig());
         assertEquals(viagens.removeLast().getEstacaoOrig(), viagensEsperadas.removeLast().getEstacaoOrig());
         
    }
    
    @Test
    public void importViagensTestEstacaoDest() throws IOException{
        DoublyLinkedList<Bilhete> viagens = inputFile.importViagens(".\\fx_viagens.txt");  

        Bilhete s1 = new Bilhete(111222333, "Z2", 1, 10);
        Bilhete s2 = new Bilhete(111333222, "Z3", 1, 2);
        Bilhete s3 = new Bilhete(111444222, "Z2", 2, 1);
        
         
         DoublyLinkedList<Bilhete> viagensEsperadas = new DoublyLinkedList<Bilhete>();
         viagensEsperadas.addLast(s1);
         viagensEsperadas.addLast(s2);
         viagensEsperadas.addLast(s3);
        

         assertEquals(viagens.removeLast().getEstacaoDest(), viagensEsperadas.removeLast().getEstacaoDest());
         assertEquals(viagens.removeLast().getEstacaoDest(), viagensEsperadas.removeLast().getEstacaoDest());
         assertEquals(viagens.removeLast().getEstacaoDest(), viagensEsperadas.removeLast().getEstacaoDest());
         
    }
    
    
    
}
