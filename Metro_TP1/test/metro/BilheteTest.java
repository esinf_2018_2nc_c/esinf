/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pmteixeira
 */
public class BilheteTest {
    
    public BilheteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNumero method, of class Bilhete.
     */
    @Test
    public void testGetNumero() {
        System.out.println("getNumero");
        Bilhete instance = new Bilhete(1,"",0,0);
        int expResult = 1;
        int result = instance.getNumero();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getTipo method, of class Bilhete.
     */
    @Test
    public void testGetTipo() {
        System.out.println("getTipo");
        Bilhete instance = new Bilhete(1,"test",1,1);
        String expResult = "test";
        String result = instance.getTipo();
        assertEquals(expResult, result);        
    }

    /**
     * Test of getEstacaoOrig method, of class Bilhete.
     */
    @Test
    public void testGetEstacaoOrig() {
        System.out.println("getEstacaoOrig");
        int expResult = 0;
        Bilhete instance = new Bilhete(1,"test",expResult,1);
        
        int result = instance.getEstacaoOrig();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getEstacaoDest method, of class Bilhete.
     */
    @Test
    public void testGetEstacaoDest() {
        System.out.println("getEstacaoOrig");
        int expResult = 0;
        Bilhete instance = new Bilhete(1,"test",expResult,1);
        
        int result = instance.getEstacaoOrig();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNumero method, of class Bilhete.
     */
    @Test
    public void testSetNumero() {
        System.out.println("setNumero");
        int numero = 1;
        Bilhete instance = new Bilhete();
        instance.setNumero(numero);
        assertEquals(numero,instance.getNumero());
    }

    /**
     * Test of setTipo method, of class Bilhete.
     */
    @Test
    public void testSetTipo() {
        System.out.println("setTipo");
        String tipo = "";
        Bilhete instance = new Bilhete();
        instance.setTipo(tipo);
        assertEquals(tipo,instance.getTipo());
    }

    /**
     * Test of setEstacaoOrig method, of class Bilhete.
     */
    @Test
    public void testSetEstacaoOrig() {
        System.out.println("setEstacaoOrig");
        int estacaoOrig = 1;
        Bilhete instance = new Bilhete();
        instance.setEstacaoOrig(estacaoOrig);
        assertEquals(estacaoOrig,instance.getEstacaoOrig());
    }

    /**
     * Test of setEstacaoDest method, of class Bilhete.
     */
    @Test
    public void testSetEstacaoDest() {
        System.out.println("setEstacaoDest");
        int estacaoDest = 1;
        Bilhete instance = new Bilhete();
        instance.setEstacaoDest(estacaoDest);
        assertEquals(estacaoDest,instance.getEstacaoDest());
    }
    
}
