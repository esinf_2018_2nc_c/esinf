/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro;

import java.io.IOException;
import java.util.ListIterator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pmteixeira
 */
public class LinhaTest {
    
    public LinhaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of getEstacaoByNum method, of class Linha.
     */
    @Test
    public void testGetEstacaoByNum() throws IOException {
        System.out.println("getEstacaoByNum");
        int i = 1;
        Controller instance = new Controller();
        instance.inputEstacoes("fx_estacoes.txt");
        Linha line = instance.getLinha();
        line.ordenaEstacoes();
        line.printEstacoes();
        Estacao expResult = line.getEstacoes().last();
        Estacao result = line.getEstacaoByNum(i);
        assertEquals(expResult, result);
    }

    /**
     * Test of getEstacoesByZona method, of class Linha.
     */
    @Test
    public void testGetEstacoesByZona() {
        System.out.println("getEstacoesByZona");
        String tipo = "C1";
        Linha instance = new Linha();
        Estacao e = new Estacao(1,"IPO","C1");   
        DoublyLinkedList<Estacao> expResult = new DoublyLinkedList();
        expResult.addFirst(e);
        instance.setEstacoes(expResult);
        DoublyLinkedList<Estacao> result = instance.getEstacoesByZona(tipo);
        assertEquals(expResult.first().getZona(), result.first().getZona());
    }

    /**
     * Test of ordenaEstacoes method, of class Linha.
     */
    @Test
    public void testOrdenaEstacoes() throws IOException {
        System.out.println("ordenaEstacoes");
        Controller cont = new Controller();
        Linha instance = new Linha();
        
        cont.inputEstacoes("fx_estacoes.txt");
        cont.inputViagens("fx_viagens.txt");
        instance.ordenaEstacoes();
        
        DoublyLinkedList<Estacao> tmp = instance.getEstacoes();
        
        ListIterator<Estacao> itz = tmp.listIterator();
        while(itz.hasNext())
        {
         
            Estacao temp = itz.next();
            temp.print();
    }
    }
    
}
