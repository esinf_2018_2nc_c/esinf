/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro;

import java.util.ArrayList;
import java.util.Iterator;


/**
 *
 * @author pmteixeira
 */
public class Zona {
    
   private String type;
   DoublyLinkedList<Estacao> estacoes;

    public Zona(){
        estacoes=new DoublyLinkedList();
    }
    
    public Zona(String tipo, DoublyLinkedList<Estacao> stations){
       type=tipo;
       estacoes=stations; 
 }
    public Zona(String tipo){
       type=tipo;
       
 }
   
    public String getTipo() {
        return type;
    }

    public DoublyLinkedList getEstacoes() {
        return estacoes;
    }

    public void setTipo(String tipo) {
        type = tipo;
    }

    public void setEstacoes(DoublyLinkedList<Estacao> stations) {
        estacoes = stations;
    }

     
}
