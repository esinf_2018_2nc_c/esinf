package metro;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException;
import static java.util.concurrent.ThreadLocalRandom.current;

/**
 *
 * @author pmteixeira
 */


public class main {
        
        
        
            public static void main(String [] args) throws IOException
            {
                //criacao do controller
                Controller controller = new Controller ();
                
                //Load ás estações e viagens
                controller.inputEstacoes("fx_estacoes.txt");
                controller.inputViagens("fx_viagens.txt");
                controller.getLinha().ordenaEstacoes();
                //Chamada do método do controller para listar o número de utilizadores por cada estação
                controller.utilizadoresPorEstacao();
                
                //Identificação dos bilhetes em transgressão
                controller.bilhetesTransgressao();
                //controller.utils();
            }
            
}



// Transgressoes
//