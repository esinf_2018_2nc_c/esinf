/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro;


/**
 *
 * @author pmteixeira
 */
public class Estacao {
    
    private int numEstacao;
    private String descricao;
    private String zona;

    public Estacao(){
        
    }
    
    public Estacao(int numEstacao, String descricao, String zona){
       this.numEstacao = numEstacao;
       this.descricao = descricao;
       this.zona = zona;

   }

    public int getNumEstacao() {
        return numEstacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getZona() {
        return zona;
    }

    public void setNumEstacao(int numEstacao) {
        this.numEstacao = numEstacao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }
    
    public void print()
    {
        System.out.print("Estacao|" + this.getNumEstacao() + "|" +this.getDescricao()+" ||");
       
    }
    
}
