/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author pmteixeira
 */
public class inputFile {
    
    
    public static DoublyLinkedList importEstacoes(String localizacao) throws FileNotFoundException, IOException{
        DoublyLinkedList<Estacao> estacoes = new DoublyLinkedList();
         BufferedReader br = new BufferedReader(new FileReader(localizacao));
         try{
             String line = br.readLine();
             
              while (line != null) {
                  
                    String[] parts = line.split(",");
                    
                    String descricao = parts[1];
                    int numEstacao = Integer.parseInt(parts[0]);
                    
                    String zona = parts[2];
             
                   Estacao estacao = new Estacao(numEstacao, descricao, zona);
                   estacoes.addLast(estacao);
                   line = br.readLine();
              }
             
             
         }catch(Exception ex){
             System.out.println("Erro inputFile: Importar Estações");
             
         }
        
         return estacoes;
        
    }
    
    public static DoublyLinkedList importViagens(String localizacao) throws FileNotFoundException{
         DoublyLinkedList<Bilhete> viagens = new DoublyLinkedList();
         BufferedReader br = new BufferedReader(new FileReader(localizacao));
         try{
             String line = br.readLine();
                
                while (line != null) {
                    //Divide a linha do ficheiro pelas virgulas
                    String[] parts = line.split(",");
                    int numero = Integer.parseInt(parts[0]);
                    
                    String tipo = parts[1];
                    
                   int estacaoOrig = Integer.parseInt(parts[2]);
                   int estacaoDest = Integer.parseInt(parts[3]);

                   Bilhete bilhete = new Bilhete(numero, tipo, estacaoOrig, estacaoDest);
                   viagens.addLast(bilhete);
                   line = br.readLine();
                }
         }catch(Exception ex){
             System.out.println("Erro inputFile: BufferedReader");
         }
         return viagens;
    }
    
    
}
