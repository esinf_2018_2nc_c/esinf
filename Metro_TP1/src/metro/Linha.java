/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro;

import java.util.Arrays;
import java.util.ListIterator;

/**
 *
 * @author pmteixeira
 */
public class Linha {
    
    DoublyLinkedList<Estacao> linha = new DoublyLinkedList();
    
    public Linha()
    {
        
    }
    
    public Linha (DoublyLinkedList<Estacao> stations){
        linha=stations;
    }
    
    public void setEstacoes (DoublyLinkedList<Estacao> stations){
        linha=stations;
    }
    
    public DoublyLinkedList<Estacao> getEstacoes ()
    {
        return linha;
    }

    void printEstacoes() {
        ListIterator<Estacao> ite = linha.listIterator();
        Estacao tmp = new Estacao();
        while(ite.hasNext())
        {
            tmp=ite.next();
            tmp.print();
        }
    }

   public Estacao getEstacaoByNum(int i)
    {
        ListIterator<Estacao> ite = linha.listIterator();
        Estacao e=new Estacao();
        Estacao temp=new Estacao();
        while(ite.hasNext())
        {
            temp=ite.next();
            if(i==temp.getNumEstacao())
            {
                e=temp;
            }
        }
        return e;
    }
   
   public DoublyLinkedList<Estacao> getEstacoesByZona(String tipo)
    {
        DoublyLinkedList<Estacao> stations = new DoublyLinkedList();
        ListIterator<Estacao> itz = linha.listIterator();
        while(itz.hasNext())
        {
            Estacao est = itz.next();
            if(est.getZona().equalsIgnoreCase(tipo))
            {
                stations.addFirst(est);
            }
        }
        return stations;
    }
   
    public void ordenaEstacoes()
    {
        int [] vec = new int[linha.size()];
        int cont=0;
        ListIterator<Estacao> ite = linha.listIterator();
        while(ite.hasNext())
        {
            vec[cont]=ite.next().getNumEstacao();
            cont++;
        }
        Arrays.sort(vec);
        DoublyLinkedList<Estacao> temp = new DoublyLinkedList();
        for(int i=1; i<vec.length; i++){
            Estacao e = getEstacaoByNum(i);
            temp.addFirst(e);
        }
        linha=temp;
        this.setEstacoes(temp);
        
    }
}
