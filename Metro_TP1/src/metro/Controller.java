/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.ListIterator;
import java.util.Map;

/**
 *
 * @author pmteixeira
 */
public class Controller{
    
    private DoublyLinkedList<Estacao> estacoes;
    private DoublyLinkedList<Bilhete> viagens;
    private DoublyLinkedList<Zona> zonas;
    private ArrayList<String> invalidos = new ArrayList<String>();
    private Linha linha;
    
    
    public Controller(){
        this.estacoes = new DoublyLinkedList();
        this.viagens = new DoublyLinkedList();
        this.zonas = new DoublyLinkedList();
        this.linha = new Linha();
        
    }
    
    public Controller(DoublyLinkedList<Estacao> estacoes, DoublyLinkedList<Bilhete> viagens){
        this.estacoes = estacoes;
        this.viagens = viagens;
        this.zonas = new DoublyLinkedList();
        linha = new Linha(estacoes);
    }
    
    public void inputEstacoes(String loc) throws IOException{
       DoublyLinkedList<Estacao> stations = inputFile.importEstacoes(loc);
        estacoes = stations;
        linha.setEstacoes(estacoes);
        
        
        ListIterator<Estacao> es = estacoes.listIterator();
        int i=0;

        while(es.hasNext())
        {
            Estacao tmp = new Estacao();
            tmp=es.next();           
            Zona zon = new Zona(tmp.getZona());
            if(zonas.isEmpty()||i>0)
            {
            if(validaZonas(zon))
            {
                zonas.addFirst(zon);
                i++;
            }
            
            }
        }
        ordenaZonas();
       
        
        ListIterator<Zona> itz = zonas.listIterator();
        DoublyLinkedList<Zona> zonatemps = new DoublyLinkedList();
        while(itz.hasNext())
        {
         
            Zona temp = itz.next();
            System.out.print("ZONA: "+ temp.getTipo()+" ");
            DoublyLinkedList<Estacao> est = linha.getEstacoesByZona(temp.getTipo());
            temp.setEstacoes(est);
            zonatemps.addFirst(temp);
            ListIterator<Estacao> ity = est.listIterator();
            while(ity.hasNext())
            {
                Estacao tp = ity.next();
                tp.print();
                System.out.println();
                
            }
            System.out.println();
            

        }
        zonas = zonatemps;
        
    }
    
    public void ordenaZonas()
    {
        String [] vec = new String[zonas.size()];
        int cont=0;
        ListIterator<Zona> ite = zonas.listIterator();
        while(ite.hasNext())
        {
            vec[cont]=ite.next().getTipo();
            cont++;
        }
        Arrays.sort(vec);
        
        
        
        DoublyLinkedList<Zona> temp = new DoublyLinkedList();
        for(int i=0; i<vec.length; i++){
            Zona z = getZonaByTipo(vec[i]);
            temp.addFirst(z);
        }
        zonas=temp;
        
        
    }
    
    public Zona getZonaByTipo(String tipo)
     {
       ListIterator<Zona> ite = zonas.listIterator();
        Zona e=new Zona();
        Zona temp=new Zona();
        while(ite.hasNext())
        {
            temp=ite.next();
            if(tipo.equalsIgnoreCase(temp.getTipo()))
            {
                e=temp;
            }
        }
        return e;
      }
      
        
    
    public boolean validaZonas(Zona zona)
    {
        ListIterator<Zona> itz = zonas.listIterator();
        
        while(itz.hasNext())
        {
            if(itz.next().getTipo().equalsIgnoreCase(zona.getTipo()))
            {
                return false;
            }
        }
        return true;
    }
    
    
    public void inputViagens(String loc) throws IOException{
       DoublyLinkedList<Bilhete> trips = inputFile.importViagens(loc);
        viagens = trips;
    }
    
   
    
    public String maiorSequenciaEstacoes (String tipo)
    {
        String s="";
        ListIterator<Estacao> itb = estacoes.listIterator();
        {
            
        }

        return s;
    }
    public void bilhetesTransgressao()
    {
        ListIterator<Bilhete> itb = viagens.listIterator();
        
        System.out.println("\nBilhetes Validos e Invalidos:\n");
        while(itb.hasNext())
        {
            Bilhete temp=itb.next();
            if(validaBilhetes(temp.getEstacaoOrig(),temp.getEstacaoDest(),temp.getTipo()))
            {
               System.out.println("Bilhete "+temp.getNumero()+" Valido");
            }
            else
            {
                System.out.println("Bilhete "+temp.getNumero()+" Invalido");
            }
            }

}
   /**
    * 
    * Verifica se um blihete se encontra em transgressão
    * @param orig estacao origem
    * @param dest estacao destino  
    * @param tipo tipo do bilhete
    * @return true em caso de valido e false em caso de invalido
    */
    public boolean validaBilhetes(int orig, int dest, String tipo)
    {
        int num = Integer.parseInt(tipo.substring(1,2));
        int zonaDestino=0, zonaOrigem=0;
        ListIterator<Estacao> ito = estacoes.listIterator();
        ListIterator<Estacao> itd = estacoes.listIterator();
        while (ito.hasNext())
        {
             Estacao tmp = ito.next();
             if(tmp.getNumEstacao()==orig)
             {
                 zonaOrigem = Integer.parseInt(tmp.getZona().substring(1, 2));
             }
        }
        
        while (itd.hasNext())
        {
             Estacao tmp = itd.next();
             if(tmp.getNumEstacao()==dest)
             {
                 zonaDestino = Integer.parseInt(tmp.getZona().substring(1, 2));
             }
        }
        
       if(num<=(Math.abs((zonaDestino-zonaOrigem))))
       {
          return false;
       }
     return true;   
    }
    
    /**
     * Imprime o número de utilizadores que passaram em determinada estação
     */
    
   
    
    
    /**
     * Itera a lista de estacoes, e imprime os seus dados, assim como o número
     * de pessoas que por lá passaram.
     */
    public void utilizadoresPorEstacao()
    {
         estacoes = linha.getEstacoes();
         ListIterator<Estacao> ite = estacoes.listIterator();
         
         
         int count;
         System.out.println("\nEstacoes : \n");
                while(ite.hasNext())
                {
                    count=0;
                    Estacao tmp = ite.next();
                    
                    tmp.print();
                    count=contaPassagens(tmp.getNumEstacao());
                    System.out.print(count+"\n");
                }
    }
    

    /**
     * Itera a lista de bilhetes e conta o numero de passagens por uma estacao em todas as viagens
     * @param estacao número da estacao
     * @return contagem do número de viagens que passaram na estacao passada por parametro
     */
    public int contaPassagens(int estacao)
    {
        int count=0;
        ListIterator<Bilhete> itb=viagens.listIterator();
        while(itb.hasNext())
                    {
                        Bilhete tmp2 = itb.next();
                        if(tmp2.getEstacaoOrig()==estacao||tmp2.getEstacaoDest()==estacao)
                            count++;
                        else
                            if(estacao>tmp2.getEstacaoOrig()&&estacao<tmp2.getEstacaoDest())
                            {
                                count++;
                            }
                    }
        return count;
    }

   

    public Linha getLinha()
    {
        return linha;
    }
    
    public DoublyLinkedList<Zona> getZonas()
    {
        return zonas;
    }
    
    public void setZonas(DoublyLinkedList<Zona> zon)
    {
        zonas=zon;
    }
    
   
    }
