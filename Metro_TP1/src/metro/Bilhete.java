/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metro;

/**
 *
 * @author pmteixeira
 */
public class Bilhete {
    
    private int numero;
    private String tipo;
    private int estacaoOrig;
    private int estacaoDest;
    
    public Bilhete(){
        
    }
    
    public Bilhete(int numero, String tipo, int estacaoOrig, int estacaoDest){
           this.numero = numero;
           this.tipo = tipo;
           this.estacaoOrig = estacaoOrig;
           this.estacaoDest = estacaoDest;
    }

    public int getNumero() {
        return numero;
    }

    public String getTipo() {
        return tipo;
    }

    public int getEstacaoOrig() {
        return estacaoOrig;
    }

    public int getEstacaoDest() {
        return estacaoDest;
    }
    

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setEstacaoOrig(int estacaoOrig) {
        this.estacaoOrig = estacaoOrig;
    }

    public void setEstacaoDest(int estacaoDest) {
        this.estacaoDest = estacaoDest;
    }

}
