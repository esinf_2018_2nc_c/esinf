/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morse_code;

/**
 *
 * @author pmteixeira
 */
public class Letter implements Comparable<Letter> {
    
    public BST<MorsePairForCode> code_tree = null;
    public String PlainStr;
    public String CodeStr;
    public int LetterCnt = 0;
    public int NonEnglishCnt = 0;
    public int NumberCnt = 0;
    public int PunctuationCnt = 0;
    
    public void SetTree(BST<MorsePairForCode> p_t)
    {
        code_tree = p_t;
    }
    
    // Alínea 4 - Complete Morse Coding
    public String CodeMorse(String morse_inp)
    {
        LetterCnt = 0;
        NonEnglishCnt = 0;
        NumberCnt = 0;
        PunctuationCnt = 0;
        PlainStr = morse_inp;
        String ret_str = new String();
        for(int i = 0 ; i < morse_inp.length(); i++)
        {
            MorsePairForCode mp = new MorsePairForCode();
            mp.AlphaRep = morse_inp.substring(i, i + 1);
            if(code_tree != null && code_tree.find(mp, code_tree.root) != null)
            {
                MorsePairForCode gd = code_tree.find(mp, code_tree.root).getElement();
                ret_str = ret_str + gd.RepMorse;
                if(i != morse_inp.length() - 1)
                    ret_str += " ";
                if("Letter".equals(gd.SymClass))
                    LetterCnt ++;
                else if("NonEnglish".equals(gd.SymClass))
                    NonEnglishCnt ++;
                else if("Number".equals(gd.SymClass))
                    NumberCnt ++;
                else if("Punctuation".equals(gd.SymClass))
                    PunctuationCnt ++;
            }
        }
        CodeStr = ret_str;
        return ret_str;
    }
    
    @Override
    public int compareTo(Letter o) {
        if(this.LetterCnt < o.LetterCnt)
            return 1;
        else if(this.LetterCnt > o.LetterCnt)
            return -1;
        else
            return 0;
    } 
}
