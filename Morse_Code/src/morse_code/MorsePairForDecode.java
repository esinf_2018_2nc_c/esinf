/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morse_code;

/**
 *
 * @author pmteixeira
 */
public class MorsePairForDecode implements Comparable<MorsePairForDecode> {
    
    public String RepMorse; // Representation Morse
    public String AlphaRep; // Alpha Representation
    public String SymClass; // Symbol 
    public MorsePairForDecode()
    {
        RepMorse = new String();
        AlphaRep = new String();
        SymClass = new String();
    }

    // CompareTo
    @Override
    public int compareTo(MorsePairForDecode o) {
        return this.RepMorse.compareTo(o.RepMorse);
    }
    
}
