/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morse_code;

/**
 *
 * @author pmteixeira
 */
public class MorsePairForCode implements Comparable<MorsePairForCode>{
    public String RepMorse; // Representation Morse
    public String AlphaRep; // Alpha Representation
    public String SymClass; // Symbol 
    public MorsePairForCode()
    {
        RepMorse = new String();
        AlphaRep = new String();
        SymClass = new String();
    }

    // Compares MorsePairForCode
    @Override
    public int compareTo(MorsePairForCode o) {
        return this.AlphaRep.compareTo(o.AlphaRep);
    }
    
}
