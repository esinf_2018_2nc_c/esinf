/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morse_code;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 *
 * @author pmteixeira
 */
public class Morse_Code {
    
    // morse_decode_tree for decode
    public static BST<MorsePairForDecode> morse_decode_tree = new BST();
    // morse_code_tree for code.
    public static BST<MorsePairForCode> morse_code_tree = new BST();
    // letter object
    public static Letter letter = new Letter();
    
    public static MorsePairForDecode[] morse_dec_codes = new MorsePairForDecode[1024];
    public static MorsePairForCode[] morse_co_codes = new MorsePairForCode[1024];
    public static int morse_cnt = 0;
    
    // Initialize Objects
    public static void Initialize()
    {
        for(int i = 0 ; i < 1024; i++)
        {
            morse_dec_codes[i] = new MorsePairForDecode();
            morse_co_codes[i] = new MorsePairForCode();
        }
    }
    
    // Read From File
    public void readFile()
    {
        try  {
            Scanner coordFile = new Scanner(new File("morse_v3_unicode.csv"));
            System.out.print("\nSuccess Opening morse_v3.csv\n");
            coordFile.useDelimiter(" ");
            // Read Lines
            
            while (coordFile.hasNext()) {
                // Read one line.
                morse_co_codes[morse_cnt].RepMorse = morse_dec_codes[morse_cnt].RepMorse = coordFile.next();
                morse_co_codes[morse_cnt].AlphaRep = morse_dec_codes[morse_cnt].AlphaRep = coordFile.next();
                morse_co_codes[morse_cnt].SymClass = morse_dec_codes[morse_cnt].SymClass = coordFile.nextLine().replaceAll(" ", "");
                
                System.out.println(morse_dec_codes[morse_cnt].RepMorse + " " + morse_dec_codes[morse_cnt].AlphaRep + " " + morse_dec_codes[morse_cnt].SymClass);
                
                morse_cnt ++;
            }
            
        }
        catch (FileNotFoundException e) {
            System.out.println("\nCan not find morse_v3.csv\n");
        }
        System.out.println(morse_cnt);
        
    }
    
     // Alínea 1 - Create BST Tree.
    public static void buildBSTTree()
    {
        for(int i = 0; i < morse_cnt; i++)
        {
            morse_decode_tree.insert(morse_dec_codes[i]);
        }
    }
    
    // Alínea 2 - Decode Sequence Morse
    public static String decodeMorse(String inp_str)
    {
        String ret_str = new String();
        StringTokenizer tokens = new StringTokenizer(inp_str, " ");
        String[] splited = new String[tokens.countTokens()];
        int index = 0;
        while(tokens.hasMoreTokens()){
            splited[index] = tokens.nextToken();
            ++index;
        }
            
        for(int i = 0; i < index; i++)
        {
            MorsePairForDecode mp = new MorsePairForDecode();
            mp.RepMorse = splited[i];
            BST.Node<MorsePairForDecode> np = morse_decode_tree.find(mp, morse_decode_tree.root());
            MorsePairForDecode gd = np.getElement();
            ret_str = ret_str + gd.AlphaRep;
        }
        return ret_str;
    }
    
    // Alínea 3 - Create Letter Tree.
    public static void buildLetterTree()
    {
        for(int i = 0; i < morse_cnt; i++)
        {
            if("Letter".equals(morse_co_codes[i].SymClass))
            {
                System.out.println("Letter Class Added " + morse_co_codes[i].RepMorse + " " + morse_co_codes[i].AlphaRep);
            }
            morse_code_tree.insert(morse_co_codes[i]);
        }
    }
    
    // Alínea 5 - Find initial sequence
    public static String findInitialSequence(String alpha1, String alpha2)
    {
        String ret_str = new String();
        MorsePairForCode mp1 = new MorsePairForCode();
        MorsePairForCode mp2 = new MorsePairForCode();
        mp1.AlphaRep = alpha1;
        mp2.AlphaRep = alpha2;
        String gd1 = morse_code_tree.find(mp1, morse_code_tree.root).getElement().RepMorse;
        String gd2 = morse_code_tree.find(mp2, morse_code_tree.root).getElement().RepMorse;
        for(int i = 0; i < gd1.length() && i < gd2.length(); i++)
        {
            if(gd1.charAt(i) == gd2.charAt(i))
            {
                ret_str += gd1.charAt(i);
            }
            else
                break;
        }
        return ret_str;
    }
    
    
    
    public static void main(String[] args) {
        
        Morse_Code p = new Morse_Code();
        
        // Initialize Objects
        Initialize();
        
        // Read File
        p.readFile();
        
        // Alínea 1 - Build BST Tree
        buildBSTTree();
        
        // Alínea 2 - Decode Sequence Morse
        System.out.println("Alínea 2 _.. To " + decodeMorse("_.. .___ .___ .___ .___"));
        System.out.println("Alínea 2 . ... .. _. .._. To " + decodeMorse(".___ .___ . ... .. _. .._. .___ .___ .___"));
        
        // Alínea 3 - Build Letter Tree
        buildLetterTree();
        letter.SetTree(morse_code_tree);
       
        // Alínea 4 - Complete Morse Coding
        System.out.println("Alínea 4 ESINF to " + letter.CodeMorse("ESINF"));
        
        // Alínea 5 - Find initial Sequences
        System.out.println("Alínea 5  " + findInitialSequence("3", "5"));
        // TODO code application logic here
    }
    
}
