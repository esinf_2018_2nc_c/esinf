
import java.io.File;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import morse_code.Letter;
import morse_code.Morse_Code;
import morse_code.Number;
import static morse_code.Morse_Code.buildBSTTree;
import static morse_code.Morse_Code.buildLetterTree;
import static morse_code.Morse_Code.decodeMorse;
import static morse_code.Morse_Code.findInitialSequence;
import static morse_code.Morse_Code.letter;
import static morse_code.Morse_Code.morse_cnt;
import static morse_code.Morse_Code.morse_co_codes;
import static morse_code.Morse_Code.morse_code_tree;
import static morse_code.Morse_Code.morse_dec_codes;
import morse_code.Utils;
import org.junit.AfterClass;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pmteixeira
 */
public class MorseTest {
    
    public MorseTest() {
    }

    @Before
    public void setUp() {
    }

    @AfterClass
    public static void tearDownClass() {
    }
    /**
     * Test of size method, of class BST.
     */
    @Test
    public void testTask1()
    {
        Morse_Code p = new Morse_Code();
        System.out.println("\nTest Task1 ----- Start -----\n");
        Morse_Code.Initialize();
        try  {
            Scanner coordFile = new Scanner(new File("morse_v3_ascii.csv"));
            System.out.print("\nReading morse_v3.csv\n");
            coordFile.useDelimiter(" ");
            // Read Lines
            
            while (coordFile.hasNext()) {
                // Read one line.
                morse_co_codes[morse_cnt].RepMorse = morse_dec_codes[morse_cnt].RepMorse = coordFile.next();
                morse_co_codes[morse_cnt].AlphaRep = morse_dec_codes[morse_cnt].AlphaRep = coordFile.next();
                morse_co_codes[morse_cnt].SymClass = morse_dec_codes[morse_cnt].SymClass = coordFile.nextLine().replaceAll(" ", "");
                
                System.out.println(morse_dec_codes[morse_cnt].RepMorse + " " + morse_dec_codes[morse_cnt].AlphaRep + " " + morse_dec_codes[morse_cnt].SymClass);
                
                morse_cnt ++;
            }
            
        }
        catch (Exception e) {
        }
        System.out.println(morse_cnt);

        assertTrue("Morse Count Must 70", morse_cnt == 70);
        System.out.println("Test Passed : Read Morse Count 70");
        // Alínea 1 Build BST Tree
        
        buildBSTTree();
        System.out.println("Test Passed : Build BST Tree");
        
        System.out.println("\nTest Task1 ----- End -----\n");
    }
    
    @Test
    public void testTask2()
    {
        System.out.println("\nTest Task2 ----- Start -----\n");
        
        // Alínea 2 - Decode Sequence Morse

        assertTrue("Decode Error", "D".equals(decodeMorse("_..")));
        assertTrue("Decode Error", "ESINF".equals(decodeMorse(". ... .. _. .._.")));
        
        System.out.println("Test Passed : Decode _.. To D");
        
        System.out.println("Test Passed : Decode . ... .. _. .._. To ESINF");
        
        System.out.println("\nTest Task2 ----- End -----\n");
    }
    
    @Test
    public void testTask3()
    {
        System.out.println("\nTest Alínea 3 e 4 ----- Start -----\n");
        
        buildLetterTree();
        System.out.println("\nTest Passed : Build Letter Tree");
        
        letter.SetTree(morse_code_tree);
        //Alínea 4 - Complete Morse Coding
        
        assertTrue("Encode Error", "_..".equals(letter.CodeMorse("D")));
        assertTrue("Encode Error", ". ... .. _. .._.".equals(letter.CodeMorse("ESINF")));
        
        System.out.println("Test Passed : Encode D To _..");
        System.out.println("Test Passed : Decode ESINF To . ... .. _. .._.");
        
    }
    
    @Test
    public void testTask5()
    {
        System.out.println("\nTest Alínea 5 \n");
        
        assertTrue("Common Sequence Error", "....".equals(findInitialSequence("5", "4")));
        System.out.println("Test Passed : Common Sequence of 5 and 4 is " + findInitialSequence("5", "4"));
    }
    
    @Test
    public void testTask6()
    {
        
        System.out.println("\nTest Task6 ----- Start -----\n");
        
        Letter[] letters = new Letter[7];
        Number[] numbers = new Number[7];
        
        for(int i = 0; i < 7; i++)
        {
            letters[i] = new Letter();
            numbers[i] = new Number();
            letters[i].SetTree(morse_code_tree);
            numbers[i].SetTree(morse_code_tree);
        }
        
        letters[0].CodeMorse("123");
        letters[1].CodeMorse("A23");
        letters[2].CodeMorse("AB3");
        letters[3].CodeMorse("ABC");
        letters[4].CodeMorse("!2C");
        letters[5].CodeMorse("!$C");
        letters[6].CodeMorse("!$=");
        
        numbers[0].CodeMorse("123");
        numbers[1].CodeMorse("A23");
        numbers[2].CodeMorse("AB3");
        numbers[3].CodeMorse("ABC");
        numbers[4].CodeMorse("!2C");
        numbers[5].CodeMorse("!$C");
        numbers[6].CodeMorse("!$=");
        
        List<Letter> list1 = Arrays.asList(letters);
        List<Letter> list1res = (List<Letter>) Utils.sortByBST(list1);
        Iterator itr= list1res.iterator();

        System.out.print("Test - Passed : Order By Class Letter : ");

        while(itr.hasNext())
        {
            Letter a = (Letter) itr.next();
            if(a.LetterCnt > 0)
                System.out.print(a.PlainStr + " ");
        }
        
        List<Number> list2 = Arrays.asList(numbers);
        List<Number> list2res = (List<Number>) Utils.sortByBST(list2);
        Iterator itr2= list2res.iterator();

        System.out.print("\nTest - Passed Order By Class Letter : ");

        while(itr2.hasNext())
        {
            Number a = (Number) itr2.next();
            if(a.NumberCnt > 0)
                System.out.print(a.PlainStr + " ");
        }
        
        System.out.println("\n\nTest Task6 ----- End -----\n");
    }
    
}
